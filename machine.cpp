/**
 * project : Engine Coolant Temp cheater
 * author  : icedfluid@gmail.com
 * date    : sept 2018
 *
 */


#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "machine.h"



/// [LEXUS IS200 1998-2004] Engine Coolant Temp Sensor specs :
int    LOOKUP_TABLE_SIZE             = 25;
double LOOKUP_ECT_TEMPERATURE     [] = {  -30,  -25,  -20,  -18,  -15,  -10,   -5,    0,    5,   10,   15,   20,   25,   30,   35,   40,   50,   60,   70,   80,   90,   95,  100,  105,  110};
double LOOKUP_ECT_VOLTAGE         [] = { 4.52, 4.36, 4.23, 4.08, 3.94, 3.84, 3.60, 3.35, 3.12, 2.89, 2.63, 2.33, 2.06, 1.86, 1.62, 1.48, 1.09, 0.85, 0.66, 0.50, 0.41, 0.34, 0.31, 0.28, 0.25};
double LOOKUP_ECT_KOHM            [] = {   25,   18,   15,   12,   10,    9,    7,  5.5,  4.5,  3.7,  3.0, 2.35,  1.9,  1.6,  1.3, 1.14, 0.75, 0.55, 0.41, 0.30, 0.24, 0.20, 0.18, 0.16, 0.14};
double LOOKUP_CHEATED_TEMPERATURE [] = {  -30,  -30,  -30,  -30,  -30,  -25,  -20,  -18,  -15,  -12,  -10,   -5,    0,   15,   20,   30,   40,   50,   70,   80,   90,   95,  100,  105,  110};


namespace DBG {

#if defined(linux)
  void trace(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    fprintf(stdout, "\n");
    vfprintf(stdout, fmt, args);
    va_end(args);
    fflush(stdout);
  }
#endif

};


namespace MATHS {

  double linearInterpolation(double x, double fa, double fb, double a, double b) {
    return (fa) + ((fb - fa) * ((x-a)/(b-a))); /// linear interpolation : p(x) = f(a) + (f(b)-f(a)) * ((x-a)/(b-a))
  }
};


class LOOKUP {

private :

  static int searchIndexLT(double * array, double v){ //voltage
    int i;
    for(i=0; i<LOOKUP_TABLE_SIZE; i++){
      if(array[i] <= v)
        break;
    }
    return i;
  }

  static int searchIndexGT(double * array, double v){ //temperature
    int i;
    for(i=0; i<LOOKUP_TABLE_SIZE; i++){
      if(array[i] >= v)
        break;
    }
    return i;
  }

  static double linearInterpolation(double x, int i, double * lookUp1, double * lookUp2) {
    return (i > 0 && i < LOOKUP_TABLE_SIZE) ? MATHS::linearInterpolation(x, lookUp1[i], lookUp1[i-1], lookUp2[i], lookUp2[i-1]) : lookUp1[0];
  }

public:

  static double resolveByVoltage(double x, double * lookUp1, double * lookUp2) {
    return LOOKUP::linearInterpolation(x, LOOKUP::searchIndexLT(lookUp2, x), lookUp1, lookUp2);
  }

  static double resolveByTemperature(double x, double * lookUp1, double * lookUp2) {
    return LOOKUP::linearInterpolation(x, LOOKUP::searchIndexGT(lookUp2, x), lookUp1, lookUp2);
  }

};


class ECT {

public:

  double voltage(){
    //return (analogRead(A0) * 0.0049); // default 10 bit resolution ADC
    return 2.63;
  }

  double temperature(double voltage) {
    return LOOKUP::resolveByVoltage(voltage, LOOKUP_ECT_TEMPERATURE, LOOKUP_ECT_VOLTAGE);
  }

};



class CHEAT {

public:

  double temperature(double ectTemperature){
    return LOOKUP::resolveByTemperature(ectTemperature, LOOKUP_CHEATED_TEMPERATURE, LOOKUP_ECT_TEMPERATURE);
  }

  double resistance(double cheatedTemperature){
    return LOOKUP::resolveByTemperature(cheatedTemperature, LOOKUP_ECT_KOHM, LOOKUP_ECT_TEMPERATURE);
  }

};


class DIGITAL_POTENTIOMETER {

public:

  void set(double resistance){
    ;
  }

};


class BYPASS {

public:

  void open(){
    ;
  }

  void close(){
    ;
  }

};


class SIGNAL {

public:

  bool start(){
    return true;
  }

  bool stop(){
    return true;
  }

};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


STEP step;
ECT ect;
CHEAT cheat;
DIGITAL_POTENTIOMETER dp;
BYPASS bypass;
SIGNAL signal;

struct {
  double ectTemperature, cheatedTemperature, cheatedKohm;
  long decreaseSteps, decreaseIter;
} vars;




STEP loop() {

  if(step == STEP_PRISTINE){
    return step = STEP_INIT;
  }

  if(step == STEP_INIT){
    memset(&vars, 0, sizeof(vars));
    return step = STEP_CHEATING;
  }

  if(step == STEP_CHEATING){

    /// read ect voltage, resolve cheated temperature and resistance
    vars.ectTemperature     = ect.temperature(ect.voltage());
    vars.cheatedTemperature = cheat.temperature(vars.ectTemperature);
    vars.cheatedKohm        = cheat.resistance(vars.cheatedTemperature);

    DBG::trace("# COMPUTED INITIAL VARS : ");
    DBG::trace("\tECT temperature     = %.1f \r\tCheated temperature = %.1f \r\tCheated KOhm        = %.2f\r", vars.ectTemperature, vars.cheatedTemperature, vars.cheatedKohm);

    /// set the resistance in digital potentiometer
    dp.set(vars.cheatedKohm*1000);

    return step = STEP_BYPASS_OPEN;
  }


  if(step == STEP_BYPASS_OPEN){
    DBG::trace("# BYPASS OPEN ...");
    /// open the bypass for ECU see cheated temperature
    bypass.open();
    return step = STEP_SIGNAL_START;
  }


  if(step == STEP_SIGNAL_START){
    if(signal.start()){
      DBG::trace("# SIGNAL START DETECTED ...");
      return step = STEP_SIGNAL_DECREASE_INIT;
    }
    usleep(5);
  }


  if(step == STEP_SIGNAL_DECREASE_INIT){
    DBG::trace("# DECREASING RESISTANCE ...");

    /// slowly decrease resistance
    vars.decreaseSteps      = (long)(vars.ectTemperature - vars.cheatedTemperature); // compute step number for decrease logic
    vars.decreaseIter       = 0;

    return step = STEP_SIGNAL_DECREASE_LOOP;
  }


  if(step == STEP_SIGNAL_DECREASE_LOOP){

    if(vars.decreaseIter < vars.decreaseSteps){

      /// compute new resistance each degree
      double r = cheat.resistance(vars.cheatedTemperature+vars.decreaseIter);
      DBG::trace("\tR = %.1f ; T = %.0f", r, vars.cheatedTemperature+vars.decreaseIter);

      /// set digital potentiometer
      dp.set(r*1000);

      vars.decreaseIter++;

      /// wait
      //usleep(waitTimeEachDegree * 1000);
      return step = STEP_SIGNAL_DECREASE_LOOP;
    }
    return step = STEP_BYPASS_CLOSE;
  }


  if(step == STEP_BYPASS_CLOSE){
    DBG::trace("# BYPASS CLOSE ...");
    /// close the bypass for ECU see cheated temperature
    bypass.close();
    return step = STEP_SIGNAL_STOP;
  }


  if(step == STEP_SIGNAL_STOP){
    if(signal.stop()){
      DBG::trace("# SIGNAL STOP DETECTED ...");
      return step = STEP_END;
    }
    usleep(5);
  }


  if(step == STEP_END){
    DBG::trace("# END !");
    exit(1);
    //return step = STEP_PRISTINE;
  }

  return step;
}



int main()
{
  while(true){ loop(); }
  return 0;
}
